﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class RoundManager {
    public static RoundState state;

    public static List<Class> players {
        private set; get;
    }
    public static List<Villager> villagers {
        private set; get;
    }
    public static List<Werewolf> werewolves {
        private set; get;
    }
    public static Witch witch {
        private set; get;
    }

    public static int innocentCount {
        get {
            return villagers.Count + ((witch != null) ? 1 : 0);
        }
    }

    public static void StartNewGame() {
        state = RoundState.werewolf;

        players = new List<Class>();
        villagers = new List<Villager>();
        werewolves = new List<Werewolf>();
        
        List<int> playerpool = new List<int>();
        foreach(ServerHandler.Player plr in ServerHandler.GetPlayers()) {
            playerpool.Add(plr.connectionId);
        }

        GameRenderer.Init(ServerHandler.GetPlayers());

        int werewolfCount = Mathf.Clamp((int)Mathf.Round(ServerHandler.playerCount / 4.0f), 1, ServerHandler.playerCount);
        Player player = null;

        for (int i = 0; i < werewolfCount; i++) {   //Werewolf
            int selected = playerpool[Random.Range(0, playerpool.Count)];
            playerpool.Remove(selected);

            player = Player.Add(ServerHandler.GetPlayerByConnectionId(selected), ClassType.werewolf);
            players.Add(player);
            werewolves.Add((Werewolf)player);
        }

        Random.Range(0, players.Count);
        if (ServerHandler.playerCount > 5) {        //Witch
            int selected = playerpool[Random.Range(0, players.Count)];
            playerpool.Remove(selected);

            player = Player.Add(ServerHandler.GetPlayerByConnectionId(selected), ClassType.witch);
            players.Add(player);
            witch = (Witch)player;
        }

        while(playerpool.Count > 0) {               //Villager
            int selected = playerpool[0];
            playerpool.Remove(selected);

            player = Player.Add(ServerHandler.GetPlayerByConnectionId(selected), ClassType.villager);
            players.Add(player);
            villagers.Add((Villager)player);
        }

        ServerHandler.StartNewGame(werewolves, villagers, witch);
        Client.InitState();
    }

    public static void StartNewGame(NewGameData gameData) {
        state = RoundState.werewolf;

        players = new List<Class>();
        villagers = new List<Villager>();
        werewolves = new List<Werewolf>();

        GameRenderer.Init(gameData.players);

        foreach (PlayerPacket werewolf in gameData.werewolves) {
            Werewolf player = (Werewolf)Player.Add(werewolf);
            werewolves.Add(player);
            players.Add(player);
        }

        foreach (PlayerPacket villager in gameData.villagers) {
            Villager player = (Villager)Player.Add(villager);
            villagers.Add(player);
            players.Add(player);
        }

        if(gameData.witch.type != ClassType.none) {
            witch = (Witch)Player.Add(gameData.witch);
            players.Add(witch);
        }

        Client.InitState();
    }

    public static void NextState() {
        FinishState();

        state = (RoundState)((((int)state) + 1) % (System.Enum.GetNames(typeof(RoundState)).Length));
        Debug.Log("Next State: " + state);
        if(state == RoundState.witch && !witch) {
            NextState();
            return;
        }

        Server.InitState();
    }

    public static void FinishState() {

    }
    
    public static class Server {
        public static List<int> deaths = new List<int>();

        public static Dictionary<int, int> werewolfTarget = new Dictionary<int, int>();

        public static void WerewolfClick(int werewolf, int target) {
            if(werewolfTarget.ContainsKey(werewolf)) {
                werewolfTarget[werewolf] = target;
            } else {
                werewolfTarget.Add(werewolf, target);
            }

            Client.WerewolfClick(werewolf, target);
            ServerHandler.SendMessage(NetworkMessages.WerewolfClick, new PlayerClick() { clickedOn = target, clickedFrom = werewolf });
        }

        public static Player ClickTarget(Dictionary<int, int> dict) {
            Dictionary<int, int> clickCounter = new Dictionary<int, int>();

            foreach (int i in dict.Values) {
                if (clickCounter.ContainsKey(i)) {
                    clickCounter[i] += 1;
                } else {
                    clickCounter.Add(i, 1);
                }
            }

            if(clickCounter.Keys.Count > 0) {
                int highest = -1;
                bool draw = false;

                foreach(int i in clickCounter.Keys) {
                    if(highest == -1) {
                        highest = i;
                    } else if (clickCounter[highest] < clickCounter[i]) {
                        highest = i;
                        draw = false;
                    } else if (clickCounter[highest] == clickCounter[i]) {
                        draw = true;
                    }
                }

                if(!draw) {
                    return Player.Get(highest);
                }
            }

            return null;
        }

        public static Dictionary<int, int> dayTarget = new Dictionary<int, int>();

        public static void DayClick(int villager, int target) {
            if (dayTarget.ContainsKey(villager)) {
                dayTarget[villager] = target;
            } else {
                dayTarget.Add(villager, target);
            }

            Client.DayClick(villager, target);
            ServerHandler.SendMessage(NetworkMessages.DayClick, new PlayerClick() { clickedOn = target, clickedFrom = villager });
        }

        public static int witchTarget = -1;

        public static void WitchClick(int witch, int target) {
            witchTarget = target;
        }

        static List<int> playersReady = new List<int>();
        public static void Continue(int id, bool ready) {
            if (ready) {
                if(!playersReady.Contains(id)) {
                    foreach(Player p in players) {
                        if(p.id == id) {    //Only if player is still alive
                            playersReady.Add(id);
                            Debug.Log("Player " + id + " is ready");
                        }
                    }
                }


                if(playersReady.Count == players.Count) {
                    Debug.Log("All players are ready");

                    switch (state) {
                        case RoundState.werewolf: {
                                Player target = Server.ClickTarget(werewolfTarget);
                                if(target) {
                                    deaths.Add(target.id);
                                    NextState();
                                } else {
                                    Debug.Log("There is no clear target");
                                }
                                break;
                            }
                        case RoundState.witch: {
                                if(witchTarget != -1) {
                                    deaths.Add(witchTarget);
                                    witchTarget = -1;
                                }
                                NextState();
                                break;
                            }
                        case RoundState.day: {
                                Player target = Server.ClickTarget(dayTarget);
                                if (target) {
                                    deaths.Add(target.id);
                                    Debug.Log("Kill " + target.name);
                                    NextState();
                                }
                                break;
                            }
                    }
                }
            } else {
                if (playersReady.Contains(id)) {
                    playersReady.Remove(id);
                }

                Debug.Log("Player " + id + " is not ready");
            }
        }

        public static void InitState() {
            playersReady = new List<int>();
            werewolfTarget = new Dictionary<int, int>();
            dayTarget = new Dictionary<int, int>();
            witchTarget = -1;

            switch (state) {
                case RoundState.werewolf: {
                        Client.InitState(deaths.ToArray());
                        ServerHandler.SendMessage(NetworkMessages.InitState, new NextState() { state = state, deaths = deaths.ToArray() });
                        deaths = new List<int>();
                        break;
                    }
                case RoundState.witch: {
                        Client.InitState(null, deaths[0]);
                        ServerHandler.SendMessage(NetworkMessages.InitState, new NextState() { state = state, werewolfTarget = deaths[0] });
                        break;
                    }
                case RoundState.day: {
                        Client.InitState(deaths.ToArray());
                        ServerHandler.SendMessage(NetworkMessages.InitState, new NextState() { state = state, deaths = deaths.ToArray() });
                        deaths = new List<int>();

                        if ((innocentCount < 2 && players.Count > 3) || innocentCount == 0) {
                            Debug.Log("Game is done");
                            ServerHandler.StopServer();
                        }
                        break;
                    }
            }
        }
    }

    public static class Client {
        public static void WerewolfClick(int werewolf, int target) {
            if (Player.local.ClassType() == ClassType.werewolf) {
                GameRenderer.WerewolfClick(werewolf, target);
            }
        }
        
        public static void DayClick(int villager, int target) {
            GameRenderer.DayClick(villager, target);
        }

        public static void WitchClick(int witch, int target) {
            GameRenderer.WitchClick(witch, target);
        }

        public static void Continue(bool ready) {
            if(ready) {
                Player.local.nextRound = true;
                GameButtonEvents.EnableButton1(false);
                GameButtonEvents.EnableButton2(false);

                if (ServerHandler.initialized) {
                    RoundManager.Server.Continue(ServerHandler.id, true);
                } else {
                    ClientHandler.SendMessage(NetworkMessages.Continue, null);
                }
            } else {
                Player.local.nextRound = false;

                if (ServerHandler.initialized) {
                    RoundManager.Server.Continue(ServerHandler.id, false);
                } else {
                    ClientHandler.SendMessage(NetworkMessages.DontContinue, null);
                }
            }
        }

        public static void InitState(int[] deaths = null, int suffering = -1) {
            if(deaths != null) {
                foreach (int id in deaths) {
                    Player player = Player.Get(id);

                    players.Remove(player);

                    switch (player.ClassType()) {
                        case ClassType.werewolf: {
                                werewolves.Remove((Werewolf)player);
                                break;
                            }
                        case ClassType.villager: {
                                villagers.Remove((Villager)player);
                                break;
                            }
                        case ClassType.witch: {
                                witch = null;
                                break;
                            }
                    }

                    GameRenderer.HighlightPlayer(player.id, Color.white);

                    Debug.Log("Surprise surprise! " + player.name + " is dead. He was " + player.ClassType());

                    MonoBehaviour.Destroy(player);
                }
            }

            if(Player.local.ClassType() == ClassType.witch) {
                ((Witch)Player.local).werewolfTarget = suffering;
            }

            switch (state) {
                case RoundState.werewolf: {
                        GameRenderer.SetBackground(Color.black);
                        break;
                    }
                case RoundState.witch: {
                        
                        break;
                    }
                case RoundState.day: {
                        GameRenderer.SetBackground();
                        break;
                    }
            }

            GameRenderer.ClearMarkers();

            Player.local.StateUpdate();
        }
    }

    public enum RoundState {
        werewolf,
        witch,
        day
    }
}