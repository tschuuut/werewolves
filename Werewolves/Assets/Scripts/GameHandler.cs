﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHandler : MonoBehaviour {
    void Start() {
        if (ServerHandler.initialized) {
            RoundManager.StartNewGame();
        }

        if(ClientHandler.initialized) {
            RoundManager.StartNewGame(ClientHandler.recivedGameData);
        }
    }
}
