﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Player : MonoBehaviour, Class {
    new public string name;
    public int id;

    public abstract ClassType ClassType();
    public abstract void StateUpdate();
    public abstract void Button1();
    public abstract void Button2();

    public bool nextRound = false;

    private static List<Player> players = new List<Player>();

    public static Player Get(int id) {
        foreach(Player player in players) {
            if(player.id == id) {
                return player;
            }
        }
        return null;
    }

    public static Player local;

    public static Player Add(ServerHandler.Player client, ClassType type) {
        GameObject obj = GameRenderer.GetPlayer(client.connectionId);

        Player player = null;

        switch (type) {
            case global::ClassType.villager: {
                    player = obj.AddComponent<Villager>();
                    break;
                }
            case global::ClassType.werewolf: {
                    player = obj.AddComponent<Werewolf>();
                    break;
                }
            case global::ClassType.witch: {
                    player = obj.AddComponent<Witch>();
                    break;
                }
        }

        player.name = client.name;

        player.id = client.connectionId;

        players.Add(player);

        if (player.name == MasterHandler.username) {
            local = player;
        }

        return player;
    }

    public static Player Add(PlayerPacket role) {
        Player player = role.ToPlayer();

        players.Add(player);
        
        if (player.name == MasterHandler.username) {
            local = player;
        }

        return player;
    }
}

public class PlayerPacket {
    public ClassType type;
    public string name;
    public int id;

    public static PlayerPacket[] FromPlayerArray(Player[] players) {
        List<PlayerPacket> packets = new List<PlayerPacket>();

        foreach (Player player in players) {
            packets.Add(FromPlayer(player));
        }

        return packets.ToArray();
    }

    public static PlayerPacket FromPlayer(Player player) {
        if(player == null) { return empty; }
        return new PlayerPacket() { type = player.ClassType(), name = player.name, id = player.id };
    }

    public Player ToPlayer() {
        GameObject obj = GameRenderer.GetPlayer(id);

        Player player = null;

        switch (type) {
            case global::ClassType.villager: {
                    player = obj.AddComponent<Villager>();
                    break;
                }
            case global::ClassType.werewolf: {
                    player = obj.AddComponent<Werewolf>();
                    break;
                }
            case global::ClassType.witch: {
                    player = obj.AddComponent<Witch>();
                    break;
                }
        }

        player.name = name;
        player.id = id;

        return player;
    }

    public static PlayerPacket empty = new PlayerPacket() { type = ClassType.none, name = "", id = -1 };
}