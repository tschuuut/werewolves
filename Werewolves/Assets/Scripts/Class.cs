﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Class {
    ClassType ClassType();
    void StateUpdate();
    void Button1();
    void Button2();
}

public enum ClassType {
    villager,
    werewolf,
    witch,
    none
}
