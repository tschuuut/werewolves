﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Werewolf : Player {
    override public ClassType ClassType() {
        return global::ClassType.werewolf;
    }

    //bool nextRound = false;   //Done in player.cs

    void Update() {
        if(local == this) {
            switch (RoundManager.state) {
                case RoundManager.RoundState.werewolf: {
                        if (Input.GetMouseButtonDown(0)) {
                            Player target = GameRenderer.MouseTarget();
                            if(target != null && target.ClassType() != global::ClassType.werewolf) {
                                GameButtonEvents.EnableButton1(true, "Continue");
                                RoundManager.Client.Continue(false);

                                if (ServerHandler.initialized) {
                                    RoundManager.Server.WerewolfClick(id, target.id);
                                    RoundManager.Client.WerewolfClick(id, target.id);
                                } else {
                                    ClientHandler.SendMessage(NetworkMessages.WerewolfClick, new PlayerClick() { clickedOn = target.id });
                                }
                            }
                        }
                        break;
                    }
                case RoundManager.RoundState.witch: {

                        break;
                    }
                case RoundManager.RoundState.day: {
                        if (Input.GetMouseButtonDown(0)) {
                            Player target = GameRenderer.MouseTarget();
                            if (target != null && target.id != id) {
                                GameButtonEvents.EnableButton1(true, "Continue");
                                RoundManager.Client.Continue(false);

                                if (ServerHandler.initialized) {
                                    RoundManager.Server.DayClick(id, target.id);
                                    RoundManager.Client.DayClick(id, target.id);
                                } else {
                                    ClientHandler.SendMessage(NetworkMessages.DayClick, new PlayerClick() { clickedOn = target.id });
                                }
                            }
                        }
                        break;
                    }
            }
        }
    }

    override public void StateUpdate() {
        nextRound = false;

        switch (RoundManager.state) {
            case RoundManager.RoundState.werewolf: {
                    GameButtonEvents.EnableButton1(false);
                    GameButtonEvents.EnableButton2(false);

                    foreach (Player player in RoundManager.players) {
                        GameRenderer.HighlightPlayer(player.id, Color.gray);
                    }

                    foreach (Player player in RoundManager.werewolves) {
                        GameRenderer.HighlightPlayer(player);
                    }
                    break;
                }
            case RoundManager.RoundState.witch: {
                    GameButtonEvents.EnableButton1(false);
                    GameButtonEvents.EnableButton2(false);

                    foreach (Player player in RoundManager.players) {
                        GameRenderer.HighlightPlayer(player.id, Color.gray);
                    }
                    GameRenderer.HighlightPlayer(this);

                    RoundManager.Client.Continue(true);
                    break;
                }
            case RoundManager.RoundState.day: {
                    GameButtonEvents.EnableButton1(false);
                    GameButtonEvents.EnableButton2(false);

                    foreach (Player player in RoundManager.players) {
                        GameRenderer.HighlightPlayer(player.id, Color.black);
                    }
                    GameRenderer.HighlightPlayer(this);
                    break;
                }
        }
    }

    override public void Button1() {
        switch (RoundManager.state) {
            case RoundManager.RoundState.werewolf: {
                    RoundManager.Client.Continue(true);
                    break;
                }
            case RoundManager.RoundState.witch: {

                    break;
                }
            case RoundManager.RoundState.day: {
                    RoundManager.Client.Continue(true);
                    break;
                }
        }
    }

    override public void Button2() {
        switch (RoundManager.state) {
            case RoundManager.RoundState.werewolf: {

                    break;
                }
            case RoundManager.RoundState.witch: {
                    
                    break;
                }
            case RoundManager.RoundState.day: {
                    
                    break;
                }
        }
    }
}
