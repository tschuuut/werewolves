﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Witch : Player {
    override public ClassType ClassType() {
        return global::ClassType.witch;
    }

    public int healPotion = 1;
    public int deathPotion = 1;

    //bool nextRound = false;   //Done in player.cs
    public int werewolfTarget;
    bool healFirst;

    void Update() {
        if (local == this) {
            switch (RoundManager.state) {
                case RoundManager.RoundState.werewolf: {

                        break;
                    }
                case RoundManager.RoundState.witch: {
                        if(!healFirst) {
                            if (Input.GetMouseButtonDown(0)) {
                                Player target = GameRenderer.MouseTarget();
                                if (target != null && target.id != id) {
                                    GameButtonEvents.EnableButton1(true, "Kill");
                                    RoundManager.Client.Continue(false);

                                    if (ServerHandler.initialized) {
                                        RoundManager.Server.WitchClick(id, target.id);
                                        RoundManager.Client.WitchClick(id, target.id);
                                    } else {
                                        ClientHandler.SendMessage(NetworkMessages.WitchClick, new PlayerClick() { clickedOn = target.id });
                                    }
                                }
                            }
                        }
                        break;
                    }
                case RoundManager.RoundState.day: {
                        if (Input.GetMouseButtonDown(0)) {
                            Player target = GameRenderer.MouseTarget();
                            if (target != null && target.id != id) {
                                GameButtonEvents.EnableButton1(true, "Continue");
                                RoundManager.Client.Continue(false);

                                if (ServerHandler.initialized) {
                                    RoundManager.Server.DayClick(id, target.id);
                                    RoundManager.Client.DayClick(id, target.id);
                                } else {
                                    ClientHandler.SendMessage(NetworkMessages.DayClick, new PlayerClick() { clickedOn = target.id });
                                }
                            }
                        }
                        break;
                    }
            }
        }
    }

    override public void StateUpdate() {
        nextRound = false;

        switch (RoundManager.state) {
            case RoundManager.RoundState.werewolf: {
                    GameButtonEvents.EnableButton1(false);
                    GameButtonEvents.EnableButton2(false);

                    foreach (Player player in RoundManager.players) {
                        GameRenderer.HighlightPlayer(player.id, Color.gray);
                    }
                    GameRenderer.HighlightPlayer(this);

                    RoundManager.Client.Continue(true);
                    break;
                }
            case RoundManager.RoundState.witch: {
                    if(healPotion > 0) {
                        healFirst = true;

                        GameButtonEvents.EnableButton1(true, "Save " + Player.Get(werewolfTarget));
                        GameButtonEvents.EnableButton2(true, "Dont Save");
                    } else {
                        DeathPotion();
                    }

                    foreach (Player player in RoundManager.players) {
                        GameRenderer.HighlightPlayer(player.id, Color.gray);
                    }
                    GameRenderer.HighlightPlayer(this);
                    break;
                }
            case RoundManager.RoundState.day: {
                    GameButtonEvents.EnableButton1(false);
                    GameButtonEvents.EnableButton2(false);

                    foreach (Player player in RoundManager.players) {
                        GameRenderer.HighlightPlayer(player.id, Color.black);
                    }
                    GameRenderer.HighlightPlayer(this);
                    break;
                }
        }
    }

    override public void Button1() {
        switch (RoundManager.state) {
            case RoundManager.RoundState.werewolf: {

                    break;
                }
            case RoundManager.RoundState.witch: {
                    if(healFirst) {
                        healPotion--;

                        if (ServerHandler.initialized) {
                            RoundManager.Server.deaths.Remove(werewolfTarget);
                        } else {
                            ClientHandler.SendMessage(NetworkMessages.Heal, new PlayerData() { connectionId = werewolfTarget });
                        }

                        DeathPotion();
                    } else {
                        deathPotion--;
                        RoundManager.Client.Continue(true);
                    }
                    break;
                }
            case RoundManager.RoundState.day: {
                    RoundManager.Client.Continue(true);
                    break;
                }
        }
    }

    override public void Button2() {
        switch (RoundManager.state) {
            case RoundManager.RoundState.werewolf: {

                    break;
                }
            case RoundManager.RoundState.witch: {
                    if (healFirst) {
                        DeathPotion();
                    } else {
                        ClientHandler.SendMessage(NetworkMessages.WitchClick, new PlayerClick() { clickedOn = -1 });
                        RoundManager.Client.Continue(true);
                    }
                    break;
                }
            case RoundManager.RoundState.day: {

                    break;
                }
        }
    }

    void DeathPotion() {
        if(deathPotion > 0) {
            GameButtonEvents.EnableButton1(false);
            GameButtonEvents.EnableButton2(true, "Dont kill");

            healFirst = false;
        } else {
            RoundManager.Client.Continue(true);
        }
    }
}
