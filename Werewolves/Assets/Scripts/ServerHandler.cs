﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public static class ServerHandler {
    public static int id {
        get {
            return NetworkServer.serverHostId;
        }
    }

    public static bool initialized {
        private set; get;
    }

    private static List<ServerHandler.Player> players = new List<ServerHandler.Player>();

    public static int playerCount {
        get {
            return players.Count;
        }
    }

    public static ServerHandler.Player GetPlayer(int i) {
        return players[i];
    }

    public static Player GetPlayerByConnectionId(int id) {
        foreach(Player player in players) {
            if(player.connectionId == id) {
                return player;
            }
        }
        return null;
    }

    public static ServerHandler.Player[] GetPlayers() {
        return players.ToArray();
    }

    public class Player {
        public Player() { }

        public Player(int id, string playername) {
            connectionId = id;
            name = playername;
        }
        public string name;
        public int connectionId;

        public PlayerData PlayerData() {
            return new PlayerData() { name = name, connectionId = connectionId };
        }
    }

    public static void StartServer(int port = 54737) {
        if (initialized) { return; }

        SceneManager.LoadScene("Lobby");

        NetworkServer.Configure(NetworkSettings.config, 16);
        NetworkServer.Listen(port);
        players.Add(new Player(NetworkServer.serverHostId, MasterHandler.username));
        NetworkServer.RegisterHandler(MsgType.Connect, PlayerConnected);
        NetworkServer.RegisterHandler(NetworkMessages.PlayerData, PlayerData);
        NetworkServer.RegisterHandler(NetworkMessages.WerewolfClick, WerewolfClick);
        NetworkServer.RegisterHandler(NetworkMessages.Continue, Continue);
        NetworkServer.RegisterHandler(NetworkMessages.DontContinue, DontContinue);
        NetworkServer.RegisterHandler(NetworkMessages.DayClick, DayClick);
        NetworkServer.RegisterHandler(NetworkMessages.Heal, Heal);
        NetworkServer.RegisterHandler(NetworkMessages.WitchClick, WitchClick);

        Debug.Log("Server Started");
        initialized = true;
    }

    public static void StopServer() {
        NetworkServer.Shutdown();
        SceneManager.LoadScene("Menu");
    }

    public static void PlayerConnected(NetworkMessage netMsg) {
        Debug.Log(netMsg.conn.connectionId + " connected");
    }

    public static void PlayerData(NetworkMessage netMsg) {
        if (SceneManager.GetActiveScene().name != "Lobby") {
            netMsg.conn.Disconnect();
            return;
        }

        PlayerData data = netMsg.ReadMessage<PlayerData>();
        data.connectionId = netMsg.conn.connectionId;

        foreach (Player player in players) {
            if (player.name == data.name) {
                netMsg.conn.Disconnect();
                return;
            }
        }

        players.Add(new Player(netMsg.conn.connectionId, data.name));
        LobbyButtonEvents.AddPlayer(data.name);
        NetworkServer.SendToAll(NetworkMessages.PlayerData, data);
        foreach (Player player in players) {
            if (player.connectionId != netMsg.conn.connectionId)
                NetworkServer.SendToClient(data.connectionId, NetworkMessages.PlayerData, player.PlayerData());
        }
    }

    public static void StartNewGame(List<Werewolf> werewolves, List<Villager> villagers, Witch witch) {
        NewGameData data = new NewGameData() { players = players.ToArray(), werewolves = PlayerPacket.FromPlayerArray(werewolves.ToArray()), villagers = PlayerPacket.FromPlayerArray(villagers.ToArray()), witch = PlayerPacket.FromPlayer(witch) };
        NetworkServer.SendToAll(NetworkMessages.NewGame, data);
    }

    public static void WerewolfClick(NetworkMessage netMsg) {
        PlayerClick data = netMsg.ReadMessage<PlayerClick>();

        Debug.Log(netMsg.conn.connectionId + " clicked on " + data.clickedOn + " (Werewolfclick)");

        RoundManager.Server.WerewolfClick(netMsg.conn.connectionId, data.clickedOn);
    }

    public static void DayClick(NetworkMessage netMsg) {
        PlayerClick data = netMsg.ReadMessage<PlayerClick>();

        Debug.Log(netMsg.conn.connectionId + " clicked on " + data.clickedOn + " (Day Vote)");

        RoundManager.Server.DayClick(netMsg.conn.connectionId, data.clickedOn);
    }

    public static void WitchClick(NetworkMessage netMsg) {
        PlayerClick data = netMsg.ReadMessage<PlayerClick>();

        Debug.Log(netMsg.conn.connectionId + " clicked on " + data.clickedOn);

        RoundManager.Server.WitchClick(netMsg.conn.connectionId, data.clickedOn);
    }

    public static void SendMessage(short msgType, MessageBase msg = null) {
        if (initialized) {
            if (msg == null) {
                NetworkServer.SendToAll(msgType, new EmptyMessage());
            } else {
                NetworkServer.SendToAll(msgType, msg);
            }
            NetworkServer.SendToAll(msgType, msg);
        } else {
            Debug.LogError("Try to send data without active server");
        }
    }

    public static void Continue(NetworkMessage netMsg) {
        RoundManager.Server.Continue(netMsg.conn.connectionId, true);
    }

    public static void DontContinue(NetworkMessage netMsg) {
        RoundManager.Server.Continue(netMsg.conn.connectionId, false);
    }

    public static void Heal(NetworkMessage netMsg) {
        PlayerData data = netMsg.ReadMessage<PlayerData>();

        RoundManager.Server.deaths.Remove(data.connectionId);
    }
}