﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LobbyButtonEvents : MonoBehaviour {
    public void StartGame() {
        SceneManager.LoadScene("Game");
    }

    public static List<string> notAddedPlayers = new List<string>();
    public static void AddPlayer(string name) {
        notAddedPlayers.Add(name);
        UpdatePlayer();
    }

    private static void UpdatePlayer() {
        if (GameObject.Find("PlayerList")) {
            Dropdown list = GameObject.Find("PlayerList").GetComponent<Dropdown>();
            while(notAddedPlayers.Count != 0) {
                string player = notAddedPlayers[0];
                list.options.Add(new Dropdown.OptionData(player));
                notAddedPlayers.Remove(player);
            }
        }
    }

    void Start() {
        if (ServerHandler.initialized) {
            AddPlayer(MasterHandler.username);
            GameObject.Find("Start").GetComponent<Button>().interactable = true;
        }
        UpdatePlayer();
    }
}
