﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public static class ClientHandler {
    public static bool initialized {
        private set; get;
    }
    private static NetworkClient client;

    public static int id;

    public static void JoinServer(string serverAddress, int port = 54737) {
        if(initialized) { return; }
        SceneManager.LoadScene("Lobby");

        client = new NetworkClient();
        client.Configure(NetworkSettings.config, 1);
        client.Connect(serverAddress, port);
        client.RegisterHandler(MsgType.Connect, ConnectedToServer);
        client.RegisterHandler(MsgType.Disconnect, DisconnectedFromServer);
        client.RegisterHandler(NetworkMessages.PlayerData, NewPlayer);
        client.RegisterHandler(NetworkMessages.NewGame, NewGame);
        client.RegisterHandler(NetworkMessages.WerewolfClick, WerewolfClick);
        client.RegisterHandler(NetworkMessages.InitState, InitState);
        client.RegisterHandler(NetworkMessages.DayClick, DayClick);

        initialized = true;
    }

    public static void ConnectedToServer(NetworkMessage netMsg) {
        PlayerData data = new PlayerData() { name = MasterHandler.username };
        client.Send(NetworkMessages.PlayerData, data);
    }
    
    public static void DisconnectedFromServer(NetworkMessage netMsg) {
        client.Shutdown();
        client = null;
        initialized = false;
        SceneManager.LoadScene("Menu");
    }

    public static void NewPlayer(NetworkMessage netMsg) {
        PlayerData data = netMsg.ReadMessage<PlayerData>();
        LobbyButtonEvents.AddPlayer(data.name);

        if (data.name == MasterHandler.username) {
            id = data.connectionId;
        }
    }

    public static NewGameData recivedGameData {
        private set; get;
    }

    public static void NewGame(NetworkMessage netMsg) {
        recivedGameData = netMsg.ReadMessage<NewGameData>();
        SceneManager.LoadScene("Game");
    }

    public static void SendMessage(short msgType, MessageBase msg) {
        if(initialized) {
            if(msg == null) {
                client.Send(msgType, new EmptyMessage());
            } else {
                client.Send(msgType, msg);
            }
        } else {
            Debug.LogError("Try to send data without connected to server");
        }
    }

    public static void WerewolfClick(NetworkMessage netMsg) {
        PlayerClick data = netMsg.ReadMessage<PlayerClick>();
        RoundManager.Client.WerewolfClick(data.clickedFrom, data.clickedOn);
    }

    public static void DayClick(NetworkMessage netMsg) {
        PlayerClick data = netMsg.ReadMessage<PlayerClick>();
        RoundManager.Client.DayClick(data.clickedFrom, data.clickedOn);
    }

    public static void InitState(NetworkMessage netMsg) {
        NextState data = netMsg.ReadMessage<NextState>();

        Debug.Log("Next State is " + data.state);

        RoundManager.state = data.state;

        RoundManager.Client.InitState(data.deaths, data.werewolfTarget);
    }
}
