﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public static class NetworkMessages {
    public static short PlayerData = MsgType.Highest + 1;
    public static short NewGame = MsgType.Highest + 2;
    public static short WerewolfClick = MsgType.Highest + 3;
    public static short Continue = MsgType.Highest + 4;
    public static short DontContinue = MsgType.Highest + 5;
    public static short InitState = MsgType.Highest + 6;
    public static short DayClick = MsgType.Highest + 7;
    public static short Heal = MsgType.Highest + 8;
    public static short WitchClick = MsgType.Highest + 9;
}

public class PlayerData : MessageBase {
    public int connectionId;
    public string name;
}

public class NewGameData : MessageBase {
    public ServerHandler.Player[] players;
    public PlayerPacket[] werewolves;
    public PlayerPacket[] villagers;
    public PlayerPacket witch;
}

public class PlayerClick : MessageBase {
    public int clickedFrom;
    public int clickedOn;
}

public class NextState : MessageBase {
    public RoundManager.RoundState state;
    public int[] deaths = new int[0];
    public int werewolfTarget;
}

public class EmptyMessage : MessageBase {

}