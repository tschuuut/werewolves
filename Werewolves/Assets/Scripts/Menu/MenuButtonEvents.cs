﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtonEvents : MonoBehaviour {
    void Start() {
#if UNITY_EDITOR
        GameObject.Find("Username").GetComponent<UnityEngine.UI.InputField>().text = "Editor";
#endif
#if !UNITY_EDITOR
        if(MasterHandler.username != "" && MasterHandler.username != null) {
            GameObject.Find("Username").GetComponent<UnityEngine.UI.InputField>().text = MasterHandler.username;
        } else {
            GameObject.Find("Username").GetComponent<UnityEngine.UI.InputField>().text = "Client ";
        }
#endif
    }

    public void HostGame() {
        MasterHandler.username = GameObject.Find("Username").GetComponent<UnityEngine.UI.InputField>().text;
        if(MasterHandler.username == "") { return; }
        ServerHandler.StartServer();
    }

    public void JoinServer() {
        MasterHandler.username = GameObject.Find("Username").GetComponent<UnityEngine.UI.InputField>().text;
        if (MasterHandler.username == "") { return; }
        ClientHandler.JoinServer("localhost");
    }
}
