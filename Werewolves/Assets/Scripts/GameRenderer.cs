﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameRenderer {
    private static Transform s_players;
    private static Transform players {
        get {
            if (s_players == null) {
                s_players = GameObject.Find("Canvas").transform.Find("Players");
            }

            return s_players;
        }
    }

    private static Transform s_markers;
    private static Transform markers {
        get {
            if (s_markers == null) {
                s_markers = GameObject.Find("Canvas").transform.Find("Markers");
            }

            return s_markers;
        }
    }

    private static GameObject prefab {
        get {
            return Resources.Load<GameObject>("Player");
        }
    }

    private static Dictionary<int, Text> objects;
    private static Color defaultColor = Color.gray;
    private static GameObject AddPlayer(int id, string name) {
        GameObject player = GameObject.Instantiate(prefab);
        player.name = id.ToString();
        player.transform.SetParent(players);
        Text text = player.GetComponent<Text>();
        text.text = name;
        text.color = defaultColor;
        objects.Add(id, text);
        return player;
    }

    static float radian = Mathf.PI * 2;
    public static void UpdatePositions() {
        for (int i = 0; i < players.childCount; i++) {
            RectTransform rect = players.GetChild(i).GetComponent<RectTransform>();
            rect.anchoredPosition = new Vector2(Mathf.Cos(radian / players.childCount * i), Mathf.Sin(radian / players.childCount * i)) * Screen.height / 2.3f;
        }
    }

    public static GameObject GetPlayer(int id) {
        return objects[id].gameObject;
    }

    public static Text GetPlayerText(int id) {
        return objects[id];
    }

    public static void HighlightPlayer(int id, Color color) {
        objects[id].color = color;
    }
    
    public static void HighlightPlayer(Player player) {
        switch(player.ClassType()) {
            case ClassType.villager: {
                    HighlightPlayer(player.id, Color.magenta);
                    break;
                }
            case ClassType.werewolf: {
                    HighlightPlayer(player.id, Color.red);
                    break;
                }
            case ClassType.witch: {
                    HighlightPlayer(player.id, Color.green);
                    break;
                }
        }
    }

    public static void Init(ServerHandler.Player[] players) {
        objects = new Dictionary<int, Text>();

        foreach(ServerHandler.Player player in players) {
            AddPlayer(player.connectionId, player.name);
        }

        UpdatePositions();
    }

    public static void ClearMarkers() {
        foreach(Image img in werewolfClickMarkers.Values) {
            GameObject.Destroy(img.gameObject);
        }

        werewolfClickMarkers = new Dictionary<int, Image>();

        foreach (Image img in dayClickMarkers.Values) {
            GameObject.Destroy(img.gameObject);
        }

        dayClickMarkers = new Dictionary<int, Image>();

        if(witchClickMarkers != null) {
            GameObject.Destroy(witchClickMarkers);

            witchClickMarkers = null;
        }
    }

    private static GameObject werewolfClickMarker {
        get {
            return Resources.Load<GameObject>("WerewolfClickMarker");
        }
    }

    public static Dictionary<int, Image> werewolfClickMarkers = new Dictionary<int, Image>();
    public static void WerewolfClick(int werewolf, int target) {
        if(werewolfClickMarkers.ContainsKey(werewolf)) {
            werewolfClickMarkers[werewolf].rectTransform.anchoredPosition = GetPlayerText(target).rectTransform.anchoredPosition + new Vector2(10 * werewolf, 0);
        } else {
            GameObject obj = GameObject.Instantiate(werewolfClickMarker);
            obj.transform.SetParent(markers);
            Image image = obj.GetComponent<Image>();
            image.rectTransform.anchoredPosition = GetPlayerText(target).rectTransform.anchoredPosition + new Vector2(20 * werewolf, 0);
            werewolfClickMarkers.Add(werewolf, image);
        }
    }

    private static GameObject dayClickMarker {
        get {
            return Resources.Load<GameObject>("DayClickMarker");
        }
    }

    public static Dictionary<int, Image> dayClickMarkers = new Dictionary<int, Image>();
    public static void DayClick(int villager, int target) {
        if (dayClickMarkers.ContainsKey(villager)) {
            dayClickMarkers[villager].rectTransform.anchoredPosition = GetPlayerText(target).rectTransform.anchoredPosition + new Vector2(10 * villager, 0);
        } else {
            GameObject obj = GameObject.Instantiate(dayClickMarker);
            obj.transform.SetParent(markers);
            Image image = obj.GetComponent<Image>();
            image.rectTransform.anchoredPosition = GetPlayerText(target).rectTransform.anchoredPosition + new Vector2(20 * villager, 0);
            dayClickMarkers.Add(villager, image);
        }
    }

    private static GameObject witchClickMarker {
        get {
            return Resources.Load<GameObject>("WitchClickMarker");
        }
    }

    public static Image witchClickMarkers;
    public static void WitchClick(int witch, int target) {
        if (witchClickMarkers != null) {
            witchClickMarkers.rectTransform.anchoredPosition = GetPlayerText(target).rectTransform.anchoredPosition;
        } else {
            GameObject obj = GameObject.Instantiate(witchClickMarker);
            obj.transform.SetParent(markers);
            Image image = obj.GetComponent<Image>();
            image.rectTransform.anchoredPosition = GetPlayerText(target).rectTransform.anchoredPosition;
            witchClickMarkers = image;
        }
    }

    public static Player MouseTarget() {
        PointerEventData cursor = new PointerEventData(EventSystem.current);
        cursor.position = Input.mousePosition;
        List<RaycastResult> objectsHit = new List<RaycastResult>();
        EventSystem.current.RaycastAll(cursor, objectsHit);
        foreach (RaycastResult obj in objectsHit) {
            int id;
            if (int.TryParse(obj.gameObject.name, out id)) {
                if(id.ToString() == obj.gameObject.name) {
                    return Player.Get(id);
                }
            }
        }

        return null;
    }

    static Color defaultBackground = new Color(0.5882352941176471f, 0.0823529411764706f, 0.207843137254902f);
    public static void SetBackground() {
        SetBackground(defaultBackground);
    }
    public static void SetBackground(Color color) {
        Camera.main.backgroundColor = color;
    }
}