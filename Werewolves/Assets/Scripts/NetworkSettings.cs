﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public static class NetworkSettings {
    private static ConnectionConfig localConfig;
    public static ConnectionConfig config {
        get {
            if (localConfig == null) {
                localConfig = new ConnectionConfig();
                reliable = config.AddChannel(QosType.Reliable);
                unreliable = config.AddChannel(QosType.StateUpdate);
                config.PacketSize = 1000;
            }
            return localConfig;
        }
    }

    public static byte reliable;
    public static byte unreliable;
}
