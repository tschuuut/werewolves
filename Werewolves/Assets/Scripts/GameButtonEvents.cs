﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameButtonEvents : MonoBehaviour {
    public void Button1() {
        Player.local.Button1();
    }

    public void Button2() {
        Player.local.Button2();
    }

    public static void EnableButton1(bool enabled, string text = "") {
        GameObject.Find("Canvas").transform.Find("Btn1").gameObject.SetActive(enabled);
        GameObject.Find("Canvas").transform.Find("Btn1").Find("Text").GetComponent<Text>().text = text;
    }
    
    public static void EnableButton2(bool enabled, string text = "") {
        GameObject.Find("Canvas").transform.Find("Btn2").gameObject.SetActive(enabled);
        GameObject.Find("Canvas").transform.Find("Btn2").Find("Text").GetComponent<Text>().text = text;
    }
}
